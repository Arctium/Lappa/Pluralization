# Lappa Pluralization

### What is Lappa Pluralization?

Lappa Pluralization is a simple pluralization library for .NET.

### Limitations

Currently there are only english nouns supported.

### Build Prerequisites
* .NET Core 3.0+ (includes .NET Standard 2.1)
